import React from 'react'

const bravo = 'GAGNÉ'

const Essais = ({ essais, win }) => (
    
    <div className="essai">
        {win === true ? bravo : 'essais: ' + essais}
    </div>
)

export default Essais
