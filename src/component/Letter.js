import React from 'react'
import styled from 'styled-components'

const secret = '__'

const Letter = ({ letter, feedback}) => (
    <Letterwrapper className="symbol">
        {feedback === 'secret' ? secret : letter}
    </Letterwrapper>
)

export default Letter

const Letterwrapper = styled.span`
margin: 5px;
`