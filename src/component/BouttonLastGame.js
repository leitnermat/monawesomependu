import React, { Component } from 'react'

export default class Boutton extends Component {
    render() {
        return (
            <button onClick={this.props.restore}>
                Restore game saved?
            </button>
        )
    }
}
