import React from 'react'
import styled from 'styled-components'

const Keyboard = ({ letter, onClick, alphabet }) => (
      <KeyboardButton onClick={() => onClick(letter)}>
        {letter}
      </KeyboardButton>
    )

export default Keyboard

const KeyboardButton = styled.button`
  height: 30px;
  width: 30px;
  background: white;
  text-align: center;
`