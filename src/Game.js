import React, { Component } from 'react';
import styled from 'styled-components';
import DataContext from './context/DataContext';
import Keyboard from './component/Keyboard';
import Letter from './component/Letter';
import Essais from './component/Essais';
import Boutton from './component/Boutton';
import BouttonSave from './component/BouttonSave';
import BouttonLastGame from './component/BouttonLastGame';
import BouttonLike from './component/BouttonLike';

export default class Game extends Component {
    state = {
        selected: [],
        essais: 0
    }
    
    static contextType = DataContext;

    componentWillMount(){
        const {allword} = this.context
        const randomIndex = Math.floor(Math.random() * 10);
        let wordToFind = allword[randomIndex];
        let lettersToFind = [];
        for(let i = 0; i < wordToFind.length; i++){
            lettersToFind.push(wordToFind[i])
        }
        this.setState({
            wordToFind: wordToFind,
            lettersToFind: lettersToFind,
        });
    }

    getFeedback(letter) {
        const { selected } = this.state
        return selected.includes(letter)
    }

    getEssai(){
        const found = this.state.lettersToFind.every(r=> this.state.selected.indexOf(r) >= 0)
        return found;
    }
    

    handleClick = letter => {
        let essais = this.state.essais
        essais++
        const selected = this.state.selected
        this.setState({selected: [...selected, letter], essais: essais});
    }

    replay = () => {
        this.setState({
            selected: [],
            essais: 0,
        })
        this.componentWillMount();
    }

    save = () => {
        localStorage.setItem('selected', JSON.stringify(this.state.selected))
        localStorage.setItem('essais', JSON.stringify(this.state.essais))
        localStorage.setItem('lettersToFind', JSON.stringify(this.state.lettersToFind))
        localStorage.setItem('wordToFind', JSON.stringify(this.state.wordToFind))
    }

    restore = () => {
        this.setState({
            selected: JSON.parse(localStorage.getItem('selected')),
            essais: JSON.parse(localStorage.getItem('essais')),
            lettersToFind: JSON.parse(localStorage.getItem('lettersToFind')),
            wordToFind: JSON.parse(localStorage.getItem('wordToFind'))
        })
    }

    renderTried() {
        return(
            this.state.selected.map((item, index) =>(
                <div key={index}>{item}</div>            ))
        )
    }
        
    render() {
        const {alphabet} = this.context;
        
        return (
            <GameContainer>
               
                <LetterContainer>     
                    { this.state.lettersToFind.map((letter, index) => (
                        <Letter
                            letter={letter}
                            feedback={this.getFeedback(letter) ? "visible" : "secret"}
                            key={index}
                        />
                    ))}
                </LetterContainer>   
                <div>
                    {alphabet.map((letter, index) => (
                        <Keyboard letter={letter} key={index} onClick={this.handleClick}/>
                    ))}
                </div>
                <Essais essais={this.state.essais} win={this.getEssai() ? true : false}/>
                <Boutton replay={this.replay}/>
                <BouttonSave save={this.save}/>
                <BouttonLastGame restore={this.restore}/>
                <BouttonLike like={this.state.like}/>
            </GameContainer>
            )
        }
}

const LetterContainer = styled.div`
    display: flex;
    justify-content: center;
`
const GameContainer = styled.div`
    text-align: center;
    margin-top: 100px;
`

