import React from 'react';

const DataContext = React.createContext({
    alphabet: ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
    allword: ['DOCKER', 'ELEPHANT', 'CAVES', 'VIGNERONS', 'REACT', 'SAPERLIPOPETTE', 'QUANTIQUE', 'PHILOSOPHIE', 'SKATEBOARD', 'MONTAGNE'],
    message: 'hello'
});

export default DataContext;